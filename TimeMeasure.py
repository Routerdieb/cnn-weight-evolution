import os  
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  
from tensorflow.keras.optimizers import *
from Mnist_networks import *
from helper import get_batch,isEndOfBatch
import time
from helper import *
import random
import math
import csv

#this should measure the time of a normal iteration an the Networks working
#set the seeds !!!
#produce the same starting network every time
optimizer = SGD(1e-2)
batch_size = 50

tests = 5
networks = [None]*tests
for network_nr in range(tests):
    tf.random.set_seed(300)
    random.seed(300)
    np.random.seed(300)
    networks[network_nr] = MNIST_Experiment1()
    networks[network_nr].compile_model(optimizer)


start_time = time.time()
networks[0]
print("--- %s seconds ---" % (time.time() - start_time))