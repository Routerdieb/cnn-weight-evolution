import os  
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  
from tensorflow.keras.optimizers import *
from Cifar_networks import *
from helper import get_batch,isEndOfBatch
import time
from helper import *
import random
import math
import csv

# see: https://stackoverflow.com/questions/30033096/what-is-lr-policy-in-caffe
def calc_lr(time):
    return 0.1* ((1-0.1)**time)

print("Starting Experiment MNIST 3 CNNS")
#Experiment 1
optimizer = SGD(0.1)
batch_size = 128
steps_max = 40000
repeat = 3

#set the seeds !!!
tf.random.set_seed(300)
random.seed(300)
np.random.seed(300)

#produce the same starting network every time
networks = [None]*repeat
for network_nr in range(repeat):
    networks[network_nr] = Cifar_net1()
    networks[network_nr].compile_model(optimizer)


#Set One
jumpAt = [12000,17000]
#Set Two
#jumpAt = [15000,18000]
#Set Three
#jumpAt = [12000,15000,18000]
#Set Four
#jumpAt = [14000,17000,20000]
initializer_conv = tf.keras.initializers.TruncatedNormal(mean=0.0, stddev=1e-4)

file_name = "Cifar_reinit_CNN_"#exp2"#exp2
epoch = 1
for repeat_index in range(repeat):
    file_path = 'M:\\results_eval'+file_name+'rep_'+str(repeat_index)+'_file.csv'
    with open(file_path,mode='w') as csv_file:
        fieldnames = ['step', 'val_accuracy']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()


        print("Calibrating experiment #" + str(repeat_index +1))
        capture_points = calc_capture_points(jumpAt)
        network = networks[repeat_index]
        network_weights_dict = {}
        shapes, segments = analye_network_conv(network.model)

        print("Starting experiment #" + str(repeat_index +1))

        for step in range(steps_max+1):
            x,y = get_batch(network.x_train,network.y_train,step,batch_size)

            if isEndOfBatch(network.x_train,network.y_train,step,batch_size):
               print("next epoch")
               network.shuffle()

            if step % 350 == 0:
                set_learning_rate(network,calc_lr(step / 350.0))

            #Show that it is still running
            step_div = 250
            if(step % step_div == 0):
                eval_score = network.eval()[1]
                writer.writerow({'step':step,'val_accuracy':eval_score})
                print(step,eval_score)
                csv_file.flush()

            network.model.train_on_batch(x,y)

            
            if(step in jumpAt):
                print("reinit")
                evolution_batch_size = 16
                length = segments[len(segments)-1][1]
                update_weight = [0.0] * length
                amount_iter = math.ceil(length / 25)
                
                values = [None]*25
                for i in range(amount_iter):
                    min_ , max_ = 25*i , 25*(i+1)

                    if(i % 100 == 0):
                        print(i/amount_iter)
                    
                    values = np.random.rand(25).astype(np.float32)

                    
                    update_weight[min_:max_] = values
                    
                #apply changes
                
                apply_shapes(network.model,update_weight,shapes,segments)
