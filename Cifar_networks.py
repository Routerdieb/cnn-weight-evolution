import numpy as np

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

from tensorflow.keras.datasets import cifar10
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import tensorflow as tf
import tensorflow.keras.backend as K

#All the Import used while testing
from tensorflow.keras.layers import *
from tensorflow.keras.activations import *
from tensorflow.keras.models import *

class CIFAR10:
    def __init__(self):
        # Load the data set
        (self.x_train, self.y_train), (self.x_test, self.y_test) = cifar10.load_data()
        self.x_train_ = None
        self.x_val = None
        self.y_train_ = None
        self.y_val = None
        # Convert to float32
        self.x_train = self.x_train.astype(np.float32)
        self.y_train = self.y_train.astype(np.float32)
        self.x_test = self.x_test.astype(np.float32)
        self.y_test = self.y_test.astype(np.float32)
        # Save important data attributes as variables
        self.train_size = self.x_train.shape[0]
        self.test_size = self.x_test.shape[0]
        self.train_splitted_size = 0
        self.val_size = 0
        self.width = self.x_train.shape[1]
        self.height = self.x_train.shape[2]
        self.depth = self.x_train.shape[3]
        self.num_classes = 10 # Constant for the data set
        self.num_features = self.width * self.height * self.depth
        # Reshape the y data to one hot encoding
        self.y_train = to_categorical(self.y_train, num_classes=self.num_classes)
        self.y_test = to_categorical(self.y_test, num_classes=self.num_classes)
        # Addtional class attributes
        self.scaler = None

    def get_train_set(self):
        return self.x_train, self.y_train

    def get_test_set(self):
        return self.x_test, self.y_test

    def shuffle(self):
        #print("shuffle")
        indices = tf.range(start=0,limit=tf.shape(self.x_train)[0],dtype=tf.int32)
        shuffeled_indices = tf.random.shuffle(indices)
        self.x_train = tf.gather(self.x_train,shuffeled_indices)
        self.y_train = tf.gather(self.y_train,shuffeled_indices)

    # def get_splitted_train_validation_set(self, validation_size=0.33):
    #     self.x_train_, self.x_val, self.y_train_, self.y_val =\
    #         train_test_split(self.x_train, self.y_train, test_size=validation_size)
    #     self.val_size = self.x_val.shape[0]
    #     self.train_splitted_size = self.x_train_.shape[0]
    #     return self.x_train_, self.x_val, self.y_train_, self.y_val

class Cifar_net1(CIFAR10):
    def compile_model(self,optimizer):
        # for the odd looking initalization of the original paper
        initializer_conv = tf.keras.initializers.TruncatedNormal(mean=0.0, stddev=1e-4)
        initializer_fc = tf.keras.initializers.TruncatedNormal(mean=0.0, stddev=0.04)
        initializer_final = tf.keras.initializers.TruncatedNormal(mean=0.0, stddev=1/192.0)
        # Define the CNN
        input_img = Input(shape=self.x_train.shape[1:])

        x = Conv2D(filters=64, kernel_size=5, padding='same',bias_initializer=initializer_conv,kernel_initializer=initializer_conv)(input_img)
        x = MaxPool2D(pool_size=(3,3),strides=(2,2))(x)
        x = BatchNormalization()(x)

        x = Conv2D(filters=64, kernel_size=5, padding='same',bias_initializer=initializer_conv,kernel_initializer=initializer_conv)(x)
        x = MaxPool2D(pool_size=(3,3),strides=(2,2))(x)
        x = BatchNormalization()(x)        

        x = Flatten()(x)
        x = Dense(units=384,kernel_initializer=initializer_fc,bias_initializer=initializer_fc)(x)
        x = Activation("relu")(x)
        x = Dense(units=192,kernel_initializer=initializer_fc,bias_initializer=initializer_fc)(x)
        x = Activation("relu")(x)
        x = Dense(units=10,kernel_initializer=initializer_final,bias_initializer=initializer_final)(x)
        #x = Activation("relu")(x)
        y_pred = Activation("softmax")(x)

        # Build the model
        self.model = Model(inputs=[input_img], outputs=[y_pred])
        self.model.summary()
        self.optimizer = optimizer

        self.model.compile(
            loss="categorical_crossentropy",
            optimizer=optimizer,
            metrics=["accuracy"])

    def fit(self,epochs,batchsize):
        self.model.fit(
            x=self.x_train, 
            y=self.y_train, 
            epochs=epochs,
            batch_size=batchsize,
            validation_data=(self.x_test, self.y_test),
            )

    def eval(self):
        return self.model.evaluate(self.x_test, self.y_test)

    def getModel(self):
        return self.model

    def trainOnBatch(self,batchX,batchY):
        self.model.train_on_batch(batchX,batchY)

c = Cifar_net1()
c.compile_model(optimizer=tf.keras.optimizers.SGD(learning_rate=1e-3))