import numpy as np

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

import tensorflow as tf
import tensorflow.python.keras.backend as K
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.image import ImageDataGenerator


def flatten_network(model):
    network_weights = model.trainable_variables
    flat_network_weights = [tf.reshape(network_weight, shape=[1, -1]) for network_weight in network_weights]
    concatenated_weights = tf.concat([flat_network_weights[0], flat_network_weights[1]], 1)
    for weights in flat_network_weights[2:]:
        concatenated_weights = tf.concat([concatenated_weights, weights], 1)
    concatenated_weights= tf.keras.backend.get_value(concatenated_weights)
    return concatenated_weights[0]

def flatten_network_conv(model):
    layers = model.layers
    all_layers = []
    for layer in layers:
        if(layer.name.startswith("conv2d")):
            w = layer.get_weights()
            weight = w[0]
            #bias = w[1]
            flat = tf.reshape(weight,(-1,1))
            all_layers.append(flat)

    concatenated_weights = tf.concat([all_layers[0], all_layers[1]], 0)
    for weights in all_layers[2:]:
        concatenated_weights = tf.concat([concatenated_weights, weights], 0)
    return concatenated_weights

def analye_network(model):
    network_weights = model.trainable_variables
    shapes = []
    segments = []
    lower_limit = 0
    for tensor in network_weights:
        shapes.append(tensor.shape)
        #calculate length
        #print(tensor.shape)
        length = 1
        for dim in tensor.shape:
            length *= dim
        segments.append((lower_limit,lower_limit+length))
        lower_limit += length
    return shapes, segments

def analye_network_conv(model):
    shapes , segments = [], []
    lower_limit = 0
    for layer in model.layers:
        if(layer.name.startswith("conv2d")):
            print(layer.name)
            w = layer.get_weights()
            weight = w[0]
            shapes.append(weight.shape)
        #calculate length
        #print(tensor.shape)
            length = 1
            for dim in weight.shape:
                length *= dim
            segments.append((lower_limit,lower_limit+length))
            lower_limit += length
    return shapes, segments



##get helper
def get_batch(x,y,index,batch_size):
    max_index = len(x)
    amount_batches = max_index / batch_size
    index = index % amount_batches
    low = int(index * batch_size)
    high = int((index + 1) * batch_size)
    if(high > max_index):
        high = max_index
    return x[low:high],y[low:high]

def isEndOfBatch(x,y,index,batch_size):
    max_index = len(x)
    amount_batches = max_index / batch_size
    index = index % amount_batches
    if index == (amount_batches -1):
        return True
    return False

def calc_capture_points(jump_points):
    previous = 0
    points = []
    for j in jump_points:
        range_ = j - previous
        t = range_ + previous
        t_7 = int((7/10) * range_) + previous
        t_4 = int((4/10) * range_) + previous
        t_0 = previous
        points.extend([t,t_7,t_4,t_0])
        previous = t
    return points

def calc_capture_points_for_point(jump_points,current_point):
    previous = 0
    points = []
    for j in jump_points:
        range_ = j - previous
        t = range_ + previous
        t_7 = int((7/10) * range_) + previous
        t_4 = int((4/10) * range_) + previous
        t_0 = previous
        if(current_point == j):
            return t,t_7,t_4,t_0
        previous = t

def apply(model,values,shapes,segments):
    for i in range(len(shapes)):
        thing = model.trainable_variables[i]
        value = values[segments[i][0]:segments[i][1]]
        tensor_ = tf.reshape(value,shape=shapes[i])
        thing.assign(tensor_)

def apply_shapes(model,values,shapes,segment):
    print("applying")
    length = len(model.trainable_variables)
    i = 0
    for l in range(length):
        current_trainable = model.trainable_variables[i]
        if(current_trainable.shape == shapes[i]):
            value = values[segment[i][0]:segment[i][1]]
            tensor_ = tf.reshape(value,shape=shapes[i])
            current_trainable.assign(tensor_)
            i += 1
        else:
            continue

def set_learning_rate(model,current_learning_rate,verbose=False):
    K.set_value(model.optimizer.learning_rate, current_learning_rate)
    K.set_value(model.optimizer.lr, current_learning_rate)
    
    if not verbose:
        print(K.get_value(model.optimizer.learning_rate))
        print(K.get_value(model.optimizer.lr))