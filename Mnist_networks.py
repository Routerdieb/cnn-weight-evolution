import os

import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical

#All the Import used while testing
from tensorflow.keras.layers import *
from tensorflow.keras.activations import *
from tensorflow.keras.models import *

class MNIST:
    model = None

    def __init__(self):
        # Dataset
        (x_train, y_train), (x_test, y_test) = mnist.load_data()

        # Cast to np.float32
        self.x_train = x_train.astype(np.float32)
        y_train = y_train.astype(np.float32)
        self.x_test = x_test.astype(np.float32)
        y_test = y_test.astype(np.float32)

        # Dataset variables
        train_size = x_train.shape[0]
        test_size = x_test.shape[0]
        self.x_train = tf.reshape(x_train,shape=(-1,28,28,1))

        # Compute the categorical classes
        self.y_train = to_categorical(y_train, num_classes=10)
        self.y_test = to_categorical(y_test, num_classes=10)

        
    def eval(self):
        return self.model.evaluate(self.x_test, self.y_test)

    def eval_on_trainset(self,verbose=1):
        return self.model.evaluate(self.x_train,self.y_train,verbose=verbose)

    def shuffle(self):
        #print("shuffle")
        indices = tf.range(start=0,limit=tf.shape(self.x_train)[0],dtype=tf.int32)
        shuffeled_indices = tf.random.shuffle(indices)
        self.x_train = tf.gather(self.x_train,shuffeled_indices)
        self.y_train = tf.gather(self.y_train,shuffeled_indices)

class MNIST_Experiment1(MNIST):
    def compile_model(self,optimizer):
        # for the odd looking initalization of the original paper
        initializer = tf.keras.initializers.TruncatedNormal(mean=0.0, stddev=0.01)
        # Define the CNN
        input_img = Input(shape=self.x_train.shape[1:])

        x = Conv2D(filters=8, kernel_size=5, padding='same',bias_initializer=initializer,kernel_initializer=initializer)(input_img)
        x = Activation("relu")(x)
        #x = tf.keras.layers.LeakyReLU(alpha=0.1)(x)
        x = MaxPool2D()(x)

        x = Conv2D(filters=64, kernel_size=5, padding='same',bias_initializer=initializer,kernel_initializer=initializer)(x)
        x = Activation("relu")(x)
        #x = tf.keras.layers.LeakyReLU(alpha=0.1)(x)
        x = MaxPool2D()(x)

        x = Flatten()(x)
        x = Dense(units=1024,kernel_initializer=initializer,bias_initializer=initializer)(x)
        x = Activation("relu")(x)
        #x = tf.keras.layers.LeakyReLU(alpha=0.1)(x)
        x = Dense(units=10,kernel_initializer=initializer,bias_initializer=initializer)(x)
        # No Activation layer at last dense, see : https://stats.stackexchange.com/questions/163695/non-linearity-before-final-softmax-layer-in-a-convolutional-neural-network
        #x = Activation("relu")(x)
        #x = tf.keras.layers.LeakyReLU(alpha=0.1)(x)
        y_pred = Activation("softmax")(x)

        # Build the model
        self.model = Model(inputs=[input_img], outputs=[y_pred])
        #self.model.summary()
        self.optimizer = optimizer

        self.model.compile(
            loss="categorical_crossentropy",
            optimizer=optimizer,
            metrics=["accuracy"])

    def fit(self,epochs,batchsize):
        self.model.fit(
            x=self.x_train, 
            y=self.y_train, 
            epochs=epochs,
            batch_size=batchsize,
            validation_data=(self.x_test, self.y_test),
            )

class MNIST_Experiment2(MNIST):
    def compile_model(self,optimizer):
        # for the odd looking initalization of the original paper
        # GlorotNormal is Xavier initialisation
        initializer = tf.keras.initializers.GlorotNormal()
        # Define the CNN
        input_img = Input(shape=self.x_train.shape[1:])

        x = Conv2D(filters=20, kernel_size=5, padding='same',bias_initializer=initializer,kernel_initializer=initializer)(input_img)
        x = Activation("relu")(x)
        x = MaxPool2D()(x)

        x = Conv2D(filters=50, kernel_size=5, padding='same',bias_initializer=initializer,kernel_initializer=initializer)(x)
        x = Activation("relu")(x)
        x = MaxPool2D()(x)

        x = Flatten()(x)
        x = Dense(units=800,kernel_initializer=initializer,bias_initializer=initializer)(x)
        x = Activation("relu")(x)
        x = Dense(units=500,kernel_initializer=initializer,bias_initializer=initializer)(x)
        x = Activation("relu")(x)
        x = Dense(units=10,kernel_initializer=initializer,bias_initializer=initializer)(x)
        #x = Activation("relu")(x)
        y_pred = Activation("softmax")(x)

        # Build the model
        self.model = Model(inputs=[input_img], outputs=[y_pred])
        self.optimizer = optimizer

        self.model.compile(
            loss="categorical_crossentropy",
            optimizer=optimizer,
            metrics=["accuracy"])

    def fit(self,epochs,batchsize):
        self.model.fit(
            x=self.x_train, 
            y=self.y_train, 
            epochs=epochs,
            batch_size=batchsize,
            validation_data=(self.x_test, self.y_test),
            )

class MNIST0(MNIST):
    def compile_model(self,optimizer):
        # for the odd looking initalization of the original paper
        # GlorotNormal is Xavier initialisation
        # Define the CNN
        input_img = Input(shape=self.x_train.shape[1:])

        x = Conv2D(filters=8, kernel_size=5, padding='same')(input_img)
        x = Activation("relu")(x)
        x = MaxPool2D()(x)

        x = Conv2D(filters=16, kernel_size=5, padding='same')(x)
        x = Activation("relu")(x)
        x = MaxPool2D()(x)

        x = Conv2D(filters=32, kernel_size=5, padding='same')(x)
        x = Activation("relu")(x)
        x = MaxPool2D()(x)
        x = Flatten()(x)
        x = Dense(units=1024,)(x)
        x = Activation("relu")(x)
        x = Dense(units=10)(x)
        #x = Activation("relu")(x)
        y_pred = Activation("softmax")(x)

        # Build the model
        self.model = Model(inputs=[input_img], outputs=[y_pred])
        self.optimizer = optimizer
        self.model.compile(
            loss="categorical_crossentropy",
            optimizer=optimizer,
            metrics=["accuracy"])

    def fit(self,epochs,batchsize):
        self.model.fit(
            x=self.x_train, 
            y=self.y_train, 
            epochs=epochs,
            batch_size=batchsize,
            validation_data=(self.x_test, self.y_test),
            )

