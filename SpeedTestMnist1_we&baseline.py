import os  
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  
from tensorflow.keras.optimizers import *
from Mnist_networks import *
from helper import get_batch,isEndOfBatch
import time
from helper import *
import random
import math
import csv
from timeit import default_timer as timer
from datetime import timedelta


print("Starting Experiment MNIST 1*")
#Experiment 1
optimizer = SGD(1e-2)
batch_size = 50
steps_max = 2501
repeat = 3

#set the seeds !!!
tf.random.set_seed(300)
random.seed(300)
np.random.seed(300)

#produce the same starting network every time
networks = [None]*repeat
for network_nr in range(repeat):
    networks[network_nr] = MNIST_Experiment1()
    networks[network_nr].compile_model(optimizer)

#load weight evolution network(s)
weight_evolution = tf.keras.models.load_model('M:\dump_N0\weigthEvolution')
jumpAt = [2500]

capture_points = calc_capture_points(jumpAt)

network = networks[repeat_index]
network_weights_dict = {}
shapes, segments = analye_network(network.model)

        for step in range(steps_max):

            x,y = get_batch(network.x_train,network.y_train,step,batch_size)

            if isEndOfBatch(network.x_train,network.y_train,step,batch_size):
                print("next epoch")
                network.shuffle()


            network.model.train_on_batch(x,y)

            if(step in capture_points):
                network_weights_dict[step] = flatten_network(network.model)
            
            if(step in jumpAt):
                #network 1
                start = timer()
                t,t_7,t_4,t_0 = calc_capture_points_for_point(jumpAt,current_point=step)
                we_batch_size = 256
                length = segments[len(segments)-1][1]
                update_weight = [0.0] * length
                amount_iter = math.ceil(length / we_batch_size)

                for repeat_index in range(amount_iter):
                    min_ , max_ = we_batch_size*repeat_index, min(length,we_batch_size*(repeat_index+1))
                    v_t = network_weights_dict[t][min_:max_]
                    v_7t = network_weights_dict[t_7][min_:max_]
                    v_4t = network_weights_dict[t_4][min_:max_]
                    v_0t = network_weights_dict[t_0][min_:max_]

                    stack = tf.stack([v_t,v_7t,v_4t,v_0t])
                    stack = tf.transpose(stack) * 1000.0
                    values = weight_evolution.predict_on_batch(stack) / 1000.0
                    update_weight[min_:max_] = values
            

                #apply changes
                apply(network.model,update_weight,shapes,segments)
                end = timer()
                print("n_we")
                print(timedelta(seconds=end-start))

                #network 2
                
