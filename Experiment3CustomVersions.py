import os  
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  
from tensorflow.keras.optimizers import *
from Cifar_networks import *
from helper import get_batch,isEndOfBatch
import time
from helper import *
import random
import math
import csv

# see: https://stackoverflow.com/questions/30033096/what-is-lr-policy-in-caffe
def calc_lr(time):
    return 0.1* ((1-0.1)**time)


print("Starting Experiment MNIST 3 CNNS")
#Experiment 1
optimizer = SGD(0.1)
batch_size = 128
steps_max = 40000
repeat = 3

#set the seeds !!!
tf.random.set_seed(300)
random.seed(300)
np.random.seed(300)

#produce the same starting network every time
networks = [None]*repeat
for network_nr in range(repeat):
    networks[network_nr] = Cifar_net1()
    networks[network_nr].compile_model(optimizer)

#load weight evolution network(s)
experiment = 2
if(experiment == 1):
    weight_evolution = [None] * 25
    for i in range(25):
        weight_evolution[i] = tf.keras.models.load_model('M:\dump_N0\weigthEvolutionCNN10'+str(i))
elif(experiment == 2):
    weight_evolution = [tf.keras.models.load_model('M:\dump_N0\weigthEvolutionCNN2')]
elif(experiment == 3):
    weight_evolution = [tf.keras.models.load_model('M:\dump_N0\weigthEvolutionCNN3')]

weight_evolution[0].summary()

#Set One
jumpAt = [12000,17000]
#Set Two
#jumpAt = [15000,18000]
#Set Three
#jumpAt = [12000,15000,18000]
#Set Four
#jumpAt = [14000,17000,20000]


file_name = "Cifar_exp1_CNN_"#exp2"#exp2
epoch = 1
for repeat_index in range(repeat):
    file_path = 'M:\\results_eval'+file_name+str(experiment)+'rep_'+str(repeat_index)+'_file.csv'
    with open(file_path,mode='w') as csv_file:
        fieldnames = ['step', 'val_accuracy']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()


        print("Calibrating experiment #" + str(repeat_index +1))
        capture_points = calc_capture_points(jumpAt)
        network = networks[repeat_index]
        network_weights_dict = {}
        shapes, segments = analye_network_conv(network.model)
  

        print("Starting experiment #" + str(repeat_index +1))

        for step in range(steps_max+1):
            x,y = get_batch(network.x_train,network.y_train,step,batch_size)


            if step % 350 == 0:
                set_learning_rate(network,calc_lr(step / 350.0))
                #for tmp_network_id in range(len(weight_evolution)):
                #    set_learning_rate(weight_evolution[tmp_network_id],calc_lr(steps),verbose=True)
                
            if isEndOfBatch(network.x_train,network.y_train,step,batch_size):
                print("next epoch")
                network.shuffle()

            #Show that it is still running
            step_div = 250
            if(step % step_div == 0):
                eval_score = network.eval()[1]
                writer.writerow({'step':step,'val_accuracy':eval_score})
                print(step,eval_score)
                csv_file.flush()

            network.model.train_on_batch(x,y)

            if(step in capture_points):
                network_weights_dict[step] = flatten_network_conv(network.model)
                #print(len(network_weights_dict[step]))
            
            if(step in jumpAt):
                print("jumping")
                t,t_7,t_4,t_0 = calc_capture_points_for_point(jumpAt,current_point=step)
                evolution_batch_size = 16
                length = segments[len(segments)-1][1]
                update_weight = [0.0] * length
                amount_iter = math.ceil(length / 25)
                #print("amount_iter"+str(amount_iter))
                
                
                values = [None]*25
                for i in range(amount_iter):
                    min_ , max_ = 25*i , 25*(i+1)
                    v_t  = network_weights_dict[t][min_:max_]
                    v_7t = network_weights_dict[t_7][min_:max_]
                    v_4t = network_weights_dict[t_4][min_:max_]
                    v_0t = network_weights_dict[t_0][min_:max_]
                    #print(max_)

                    if(i % 100 == 0):
                        print(i/amount_iter)
                    

                
                    if(experiment == 1):
                        stack = tf.concat([v_t,v_7t,v_4t,v_0t],axis = 1)
                        stack = tf.reshape(stack,(-1,100))
                        for i in range(5):
                            for j in range(5):
                               # print("before")
                                #print(tf.shape(stack))
                            
                                values[i*5+j] = weight_evolution[i*5+j].predict_on_batch(stack)
                                #print("after")
                    elif(experiment == 2):
                        for i in range(5):
                            for j in range(5):
                                stack = tf.stack([v_t[i*5+j],v_t[i*5+j],v_t[i*5+j],v_t[i*5+j]],axis=1)
                                np_stack = np.array(stack)
                                np_stack = np.append(v_t,np_stack)
                                tens = tf.convert_to_tensor(np_stack)
                                tens = tf.reshape(tens,(29,1))
                                tens = tf.transpose(tens)
                                values[i*5+j] = weight_evolution[0].predict_on_batch(tens)
                                #print(i,j)
                        
                    elif(experiment == 3):
                        stack = tf.concat([v_t,v_7t,v_4t,v_0t],axis = 1)
                        stack = tf.reshape(stack,(-1,100))
                        #jjprint("here1")
                        values = weight_evolution[0].predict_on_batch(stack)[0]
                        #print(len(values))
                        #print(values)
                        #print("here2")
                    
                    update_weight[min_:max_] = values
                    #print(update_weight[min_:max_])
                #apply changes
                apply_shapes(network.model,update_weight,shapes,segments)
