import os

import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical

#All the Import used while testing
from tensorflow.keras.layers import *
from tensorflow.keras.activations import *
from tensorflow.keras.models import *



class MNIST0:
    model = None

    def eval(self):
        return self.model.evaluate(self.x_test, self.y_test)

    def eval_on_trainset(self,verbose=1):
        return self.model.evaluate(self.x_train,self.y_train,verbose=verbose)

    def shuffle(self):
        #print("shuffle")
        indices = tf.range(start=0,limit=tf.shape(self.x_train)[0],dtype=tf.int32)
        shuffeled_indices = tf.random.shuffle(indices)
        self.x_train = tf.gather(self.x_train,shuffeled_indices)
        self.y_train = tf.gather(self.y_train,shuffeled_indices)

class weight_evolution(MNIST_train):
    def compile_model(self,optimizer):
        # for the odd looking initalization of the original paper
        initializer = tf.keras.initializers.TruncatedNormal(mean=0.0, stddev=0.01)
        # Define the CNN
        input_img = Input(shape=(None,4))

        x = Dense(units=40,kernel_initializer=initializer,bias_initializer=initializer)(x)
        x = Activation("relu")(x)

        y_pred = Dense(units=1,kernel_initializer=initializer,bias_initializer=initializer)(x)

        # Build the model
        self.model = Model(inputs=[input_img], outputs=[y_pred])
        self.model.summary()
        self.optimizer = optimizer

        self.model.compile(
            loss=tf.keras.losses.MeanAbsoluteError(),
            optimizer=optimizer,
            metrics=["accuracy"])

    def fit(self,epochs,batchsize):
        self.model.fit(
            x=self.x_train, 
            y=self.y_train, 
            epochs=epochs,
            batch_size=batchsize,
            validation_data=(self.x_test, self.y_test),
            )

class weight_evolution_CNN2(MNIST_train):
    def compile_model(self,optimizer):
        # Define the CNN
        input_img = Input(shape=(None,29))

        x = Dense(units=100)(x)
        x = Activation("relu")(x)
        x = Dense(units=200)(x)
        x = Activation("relu")(x)
        x = Dense(units=500)(x)
        x = Activation("relu")(x)

        y_pred = Dense(units=1)(x)

        # Build the model
        self.model = Model(inputs=[input_img], outputs=[y_pred])
        self.model.summary()
        self.optimizer = optimizer

        self.model.compile(
            loss=tf.keras.losses.MeanAbsoluteError(),
            optimizer=optimizer,
            metrics=["accuracy"])

    def fit(self,epochs,batchsize):
        self.model.fit(
            x=self.x_train, 
            y=self.y_train, 
            epochs=epochs,
            batch_size=batchsize,
            validation_data=(self.x_test, self.y_test),
            )


class weight_evolution_CNN1(MNIST_train):
    def compile_model(self,optimizer):
        # Define the CNN
        input_img = Input(shape=(None,100))

        x = Dense(units=250)(x)
        x = Activation("relu")(x)
        x = Dense(units=500)(x)
        x = Activation("relu")(x)
        y_pred = Dense(units=1)(x)

        # Build the model
        self.model = Model(inputs=[input_img], outputs=[y_pred])
        self.model.summary()
        self.optimizer = optimizer

        self.model.compile(
            loss=tf.keras.losses.MeanAbsoluteError(),
            optimizer=optimizer,
            metrics=["accuracy"])

    def fit(self,epochs,batchsize):
        self.model.fit(
            x=self.x_train, 
            y=self.y_train, 
            epochs=epochs,
            batch_size=batchsize,
            validation_data=(self.x_test, self.y_test),
            )

class weight_evolution_CNN3(MNIST_train):
    def compile_model(self,optimizer):

        # Define the CNN
        input_img = Input(shape=(None,100))

        x = Dense(units=250)(x)
        x = Activation("relu")(x)
        x = Dense(units=500)(x)
        x = Activation("relu")(x)
        y_pred = Dense(units=25)(x)

        # Build the model
        self.model = Model(inputs=[input_img], outputs=[y_pred])
        self.model.summary()
        self.optimizer = optimizer

        self.model.compile(
            loss=tf.keras.losses.MeanAbsoluteError(),
            optimizer=optimizer,
            metrics=["accuracy"])

    def fit(self,epochs,batchsize):
        self.model.fit(
            x=self.x_train, 
            y=self.y_train, 
            epochs=epochs,
            batch_size=batchsize,
            validation_data=(self.x_test, self.y_test),
            )